name "java"
description "java"
default_attributes(
    "sbt" => {
            "version" => "0.13.6"
    }
)
run_list(
  "recipe[java]",
  "recipe[maven]",
  "recipe[scala]",
  "recipe[chef-sbt]"
)

