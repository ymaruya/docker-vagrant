name "java"
description "java"
run_list(
  "recipe[scala]",
  "recipe[chef-sbt]"
)
default_attributes(
  "scala" => {
    "version" => "2.10.2"
 },
  "sbt" => {
    "version" => "0.13.1"
  }
)

