#
# Cookbook Name:: pylern2
# Attributes:: default
#
# Copyright 2014, Maruya Yoshihisa
#
# All rights reserved - Do Not Redistribute
default[:scalding_examples][:install_path]  = "/usr/local/scalding_examples"