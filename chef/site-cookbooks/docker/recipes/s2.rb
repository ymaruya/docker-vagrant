#
# Cookbook Name:: docker for debian7
# Recipe:: s2
#
# Copyright 2014, Maruya Yoshihisa<m0029.swim@gmail.com>
#
# All rights reserved - Do Not Redistribute
#
#include_recipe "apt"

bash "install_docker" do
    user "root"
    cwd "/tmp"
    code <<-EOH
    apt-get -y install -qqy ca-certificates
    apt-get -y install apt-transport-https
    sh -c "wget -qO- https://get.docker.io/gpg | apt-key add -"
    sh -c "echo 'deb https://get.docker.io/ubuntu docker main' | tee /etc/apt/sources.list.d/docker.list"
    apt-get update &&  apt-get -y install -qqy lxc-docker
    EOH
end


