#
# Cookbook Name:: docker for debian7
# Recipe:: s1
#
# Copyright 2014, Maruya Yoshihisa<m0029.swim@gmail.com>
#
# All rights reserved - Do Not Redistribute
#
#include_recipe "apt"

bash "install_docker" do
    user "root"
    cwd "/tmp"
    code <<-EOH
    sh -c "echo deb http://ftp.jp.debian.org/debian wheezy-backports main | tee  /etc/apt/sources.list.d/wheezy-backports.list"
    apt-get update
    apt-get -t wheezy-backports install linux-image-amd64 -y
    EOH
end

#You need to reboot before running s2.rb

