#
# Cookbook Name:: word2vec
# Recipe:: default
#
# Copyright 2014, Maruya Yoshihisa
#
# All rights reserved - Do Not Redistribute

include_recipe "subversion"

bash "install-word2vec" do
    user "root"
    cwd "/tmp"
    code <<-EOH
    svn co http://svn.apache.org/repos/asf/couchdb/trunk  #{node[:word2vec][:install_path]}
    cd #{node[:word2vec][:install_path]}
    make
    EOH
end

