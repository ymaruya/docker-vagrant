#
# Cookbook Name:: pylern2
# Attributes:: default
#
# Copyright 2014, Maruya Yoshihisa
#
# All rights reserved - Do Not Redistribute
default[:word2vec][:install_path]  = "/usr/local/word2vec"